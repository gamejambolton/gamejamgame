﻿using UnityEngine;
using System.Collections;

public class Levers : MonoBehaviour {

	public static bool yellow;
	public static bool blue;

	// Use this for initialization
	void Start ()
	{
		yellow = true;
		blue = false;
	}

	public static void SwitchLever()
	{
		if (yellow)
		{
			yellow = false;
			blue = true;
		} else 
		{
			yellow = true;
			blue = false;
		}
	}

	public static void LeverSectionComplete()
	{
		yellow = true;
		blue = true;
	}

}
