﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {
	public GameObject Target;
	public GameObject Target2;
	public Vector3 offset;
	public float distancex;
	public float distancez;
	public PlayerMovement TargetScript;
	public PlayerMovement TargetScript2;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = Target.transform.position + (Target2.transform.position - Target.transform.position)/2 + offset;
		distancex = Target2.transform.position.x - Target.transform.position.x;
		distancez = Target2.transform.position.z - Target.transform.position.z;
//		if (distancex >= 25) {
//			TargetScript.Restrict(false, false, true, false);
//			TargetScript2.Restrict(false, false, false, true);
//		}
//		if (distancex <= -25) {
//			TargetScript.Restrict(false, false, false, true);
//			TargetScript2.Restrict(false, false, true, false);
//		}
//		if (distancez >= 15) {
//			TargetScript.Restrict(false, true, false, false);
//			TargetScript2.Restrict(true, false, false, false);
//		}
//		if (distancez >= 25) {
//			TargetScript.Restrict(true, false, false, false);
//			TargetScript2.Restrict(false, true, false, false);
//		}
//		TargetScript.Restrict(false, false, false, false);
//		TargetScript2.Restrict(false, false, false, false);
	}
}
