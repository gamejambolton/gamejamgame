﻿using UnityEngine;
using System.Collections;

public class PlayerShoot : MonoBehaviour {
	public int damagePerShot = 20;                  
	public float timeBetweenBullets = 0.30f;       
	public float range = 100f;                      

	float timer;                                    
	Ray shootRay;                                   
	RaycastHit shootHit;                            
//	ParticleSystem gunParticles;                    
	LineRenderer gunLine;                           
	float effectsDisplayTime = 0.2f;                

	void Awake () {
		gunLine = GetComponent<LineRenderer>();
//		gunParticles = GetComponent<ParticleSystem> ();
	}
	
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;
		
		if (this.tag == "Player 1 Gun") {
			if (Input.GetAxis ("Fire1") != 0 && timer >= timeBetweenBullets) {
				Debug.Log ("Fire");
				Shoot ();
			}
		}
		if (this.tag == "Player 2 Gun") {
			if (Input.GetAxis ("Fire2") != 0 && timer >= timeBetweenBullets) {
				Debug.Log ("Fire");
				Shoot ();
			}
		}
		
		if(timer >= timeBetweenBullets * effectsDisplayTime)
		{
			
			DisableEffects ();
		}
	}
	
	public void DisableEffects ()
	{

		gunLine.enabled = false;
//		gunParticles.enableEmission = false;

	}
	
	void Shoot ()
	{
		timer = 0f;
		Debug.Log ("Fire");
//		gunParticles.Stop ();
//		gunParticles.Play ();
		

		gunLine.enabled = true;
		gunLine.SetPosition (0, transform.position);
		

		shootRay.origin = transform.position;
		shootRay.direction = transform.forward;
		
		
		if(Physics.Raycast (shootRay, out shootHit, range))
		{
			EnemyHealth enemyHealth = shootHit.collider.GetComponent <EnemyHealth> ();

			if(enemyHealth != null)
			{
				
				enemyHealth.TakeDamage(damagePerShot);
			}
			

			gunLine.SetPosition (1, shootHit.point);
		}
	
		else
		{
			
			gunLine.SetPosition (1, shootRay.origin + shootRay.direction * range);
		}
	}
}
