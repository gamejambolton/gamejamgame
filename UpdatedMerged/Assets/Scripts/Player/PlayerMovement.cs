﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {
	public float turnSmoothing = 15f;
	public float speedDampTime = 0.1f;
	
	public GameObject cam;
	public Transform transform;
	
	public float speed = 3.0F;
	public float gravity = 9.0F;

//	public bool stopTop = false;
//	public bool stopBottom = false;
//	public bool stopRight = false;
//	public bool stopLeft = false;

	private Vector3 moveDirection = Vector3.zero;
	private Vector3 turnDirection = Vector3.zero;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		CharacterController controller = GetComponent<CharacterController>();
		float y = moveDirection.y;
		if (this.tag == "Player 1") {
			turnDirection = new Vector3 (Input.GetAxis ("RHorizontal"), 0, Input.GetAxis ("RVertical"));

			moveDirection = new Vector3 (Input.GetAxis ("Horizontal"), 0, Input.GetAxis ("Vertical"));
		}
		if (this.tag == "Player 2") {
			turnDirection = new Vector3 (Input.GetAxis ("RHorizontal2"), 0, Input.GetAxis ("RVertical2"));
			
			moveDirection = new Vector3 (Input.GetAxis ("Horizontal2"), 0, Input.GetAxis ("Vertical2"));
		}
		//moveDirection = transform.TransformDirection (moveDirection);
		moveDirection *= speed;
		moveDirection.y = y;
		moveDirection.y -= gravity * Time.deltaTime;


		if (turnDirection.magnitude > 0.25) {
			transform.rotation = Quaternion.LookRotation (turnDirection, Vector3.up);
		}
	
		controller.Move(moveDirection * Time.deltaTime);



	}

	void LateUpdate(){
		Vector3 viewportCoord = Camera.main.WorldToViewportPoint (transform.position);
		if (viewportCoord.x < 0.0f) {
			viewportCoord.x = 0.1f;
			this.transform.position = Camera.main.WorldToViewportPoint(viewportCoord);
		} else if (viewportCoord.x > 1.0f) {
			viewportCoord.x = 0.9f;
			this.transform.position = Camera.main.WorldToViewportPoint(viewportCoord);
		}
		if (viewportCoord.y < 0.0f) {
			viewportCoord.y = 0.1f;
			this.transform.position = Camera.main.WorldToViewportPoint(viewportCoord);
		} else if (viewportCoord.y > 1.0f) {
			viewportCoord.y = 0.9f;
			this.transform.position = Camera.main.WorldToViewportPoint(viewportCoord);
		}
	}

	void Turn()
	{
		
		Vector3 targetDirection = moveDirection;
		//targetDirection.y = -0.01f;
		targetDirection.Normalize();  
		transform.rotation = Quaternion.LookRotation(targetDirection, Vector3.up);

		
	}

//	public void Restrict(bool top, bool bottom, bool left, bool right){
//		stopTop = top;
//		stopBottom = bottom;
//		stopLeft = left;
//		stopRight = right;
//	}

//	Vector3 CheckRestrict(Vector3 moveDirection){
//		if (moveDirection.x > 0 && stopRight == true) {
//			moveDirection.x = 0;
//			return moveDirection;
//		}
//		else if (moveDirection.x < 0 && stopLeft == true) {
//			moveDirection.x = 0;
//			return moveDirection;
//		}
//		else if (moveDirection.z > 0 && stopTop == true) {
//			moveDirection.z = 0;
//			return moveDirection;
//		}
//		else if (moveDirection.z > 0 && stopBottom == true) {
//			moveDirection.z = 0;
//			return moveDirection;
//		} else {
//			return moveDirection;
//		}
//	}
}
