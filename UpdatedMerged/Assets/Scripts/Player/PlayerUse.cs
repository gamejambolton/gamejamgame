﻿using UnityEngine;
using System.Collections;

public class PlayerUse : MonoBehaviour {

	private bool nearLever;


	// Use this for initialization
	void Start () 
	{
		nearLever = false;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetButtonDown ("Use") && nearLever) 
		{
			Levers.SwitchLever();
		}
	}

	void OnTriggerEnter(Collider col)
	{
		if(col.gameObject.tag == "Lever")
		{
			nearLever = true;
		}
		
	}

	void OnTriggerExit(Collider col)
	{
		if(col.gameObject.tag == "Lever")
		{
			nearLever = false;
		}
		
	}
}
