﻿using UnityEngine;
using System.Collections;

public class EnemyMovement : MonoBehaviour {

	public float MaxDistance;

	// Use this for initialization
	void Start () {
	


	}
	
	// Update is called once per frame
	void Update () {
	
		//if (this.GetComponent<NavMeshAgent> ().hasPath) {
			Vector3 temp = GetNearestPlayer ();
			if (Vector3.Distance (temp, this.gameObject.transform.position) > MaxDistance) {
				this.GetComponent<NavMeshAgent> ().destination = temp;
			}
		//}

	}

	private Vector3 GetNearestPlayer(){
		
		GameObject Player1;
		Player1 = GameObject.FindGameObjectWithTag ("Player 1");
		GameObject Player2;
		Player2 = GameObject.FindGameObjectWithTag ("Player 2");

		float p1_Distance = 0;
		float p2_Distance = 0;
		
		p1_Distance = Vector3.Distance (Player1.transform.position, gameObject.transform.position);
		p2_Distance = Vector3.Distance (Player2.transform.position, gameObject.transform.position);
		
		if (p1_Distance < p2_Distance) {
			return Player1.transform.position;
		} else {
			return Player2.transform.position;
		}
		
	}

}
