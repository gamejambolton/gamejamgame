﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class SpawnPointArray{

	public GameObject EnemyPrefab;
	public GameObject SpawnPoint;
	[Range(0,5)]
	public float SpawnDelay;
	public bool isActive;

}

public class EnemySpawner : MonoBehaviour
{

	//Gives an instance of a static object to access the class
	private static EnemySpawner _privInstance ;
	public static EnemySpawner _instance {
		get {
			if (_privInstance == null) {
				_privInstance = GameObject.FindObjectOfType<EnemySpawner> ();
				
				//Tell unity not to destroy this object when loading a new scene!
				DontDestroyOnLoad (_privInstance.gameObject);
			}
			
			return _privInstance;
		}
	}
	//Array that stores all spawnpoints
	public SpawnPointArray[] SpawnPoints = new SpawnPointArray[1];

	// Use this for initialization
	void Start ()
	{


	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	public IEnumerator EnemyCoroutine (SpawnPointArray SpawnPoint, int newSpawnMax)
	{

		//SpawnPoint.SpawnMax = newSpawnMax;

		for (int i = 0; i < newSpawnMax; i++) {

			GameObject temp;
			temp = (GameObject)Instantiate (SpawnPoint.EnemyPrefab);
			temp.transform.parent = GameObject.Find ("Enemies").transform;
			temp.name = SpawnPoint.EnemyPrefab.name;
			temp.gameObject.GetComponent<NavMeshAgent>().enabled = false;
			temp.transform.position = SpawnPoint.SpawnPoint.transform.position + (temp.transform.up * 1.5f);

			temp.gameObject.GetComponent<NavMeshAgent>().enabled = true;
			yield return new WaitForSeconds (SpawnPoint.SpawnDelay);

		}

	}

	public void SetActive(int index, bool active){

		SpawnPoints [index].isActive = active;
	
	}

	public void SpawnEnemy(SpawnPointArray SpawnPoint, int newSpawnMax){

		StartCoroutine(EnemyCoroutine (SpawnPoint, newSpawnMax));

	}

}
