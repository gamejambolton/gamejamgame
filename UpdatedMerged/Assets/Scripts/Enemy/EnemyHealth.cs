﻿using UnityEngine;
using System.Collections;

public class EnemyHealth : MonoBehaviour {
	public int Health = 100;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void TakeDamage(int damage){
		Health = Health - damage;
		if (Health <= 0) {
			Die ();
		}
	}

	void Die(){
		Destroy (this.gameObject);
	}
}
