﻿using UnityEngine;
using System.Collections;

public class EnemyCollision : MonoBehaviour {

	[Range(0,10000)]
	public float EnemyPlayerCollisionForce;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	Vector3 GetImpulseDirection(Collision col){

		return this.gameObject.transform.forward * EnemyPlayerCollisionForce;

	}

	void OnCollisionEnter(Collision col){

		switch (col.gameObject.tag) {
		
		case "Player":
			col.gameObject.GetComponent<Rigidbody>().AddForce(GetImpulseDirection(col));
			break;
		
		}

	}

}
